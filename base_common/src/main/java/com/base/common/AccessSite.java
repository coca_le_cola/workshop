package com.base.common;



/**
 * @Title  访问端信息
 * @Author 覃忠君 on 2017/12/20.
 * @Copyright © 长笛龙吟
 */
public class AccessSite extends BaseModel {
    public static final String[] mobile_type = {"MQQBrowser","Android", "iPhone", "iPod", "iPad", "Windows Phone"};

    private boolean isApp;  // 是否移动端  true  是   false  否
    private String deviceName;  //设备名称

    public AccessSite(){}

    public AccessSite(boolean isApp, String deviceName) {
        this.isApp = isApp;
        this.deviceName = deviceName;
    }

    public String getDeviceName() {
        return deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    public boolean isApp() {
        return isApp;
    }

    public void setApp(boolean app) {
        isApp = app;
    }
}
