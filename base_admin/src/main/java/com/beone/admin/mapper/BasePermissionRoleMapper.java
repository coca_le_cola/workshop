package com.beone.admin.mapper;

import com.base.SuperMapper;
import com.beone.admin.entity.BasePermissionRole;
import com.beone.admin.mapper.provider.BasePermissionRoleMapperProvider;
import org.apache.ibatis.annotations.InsertProvider;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 运维数据_角色对应的权限配置 Mapper 接口
 * @Author 覃球球
 * @Version 1.0 on 2018-01-25
 * @Copyright 长笛龙吟
 */
public interface BasePermissionRoleMapper extends SuperMapper<BasePermissionRole> {

    /**
     * 批量插入角色与权限关系
     * @param items
     */
    @InsertProvider(type = BasePermissionRoleMapperProvider.class, method = "insertBatchPermissionRole")
    void insertBatchPermissionRole(@Param("list") List<BasePermissionRole> items);
}
