package com.beone.admin.filter;

import com.beone.admin.common.AdminConstants;
import com.base.common.util.StringRegexUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Arrays;
import java.util.Map;

/**
 * @Title  请求参数过滤器
 * @Author 覃忠君 on 2018/1/31.
 * @Copyright © 长沙科权
 */
public class GloableFilter implements Filter {
    private static final Logger log = LoggerFactory.getLogger(GloableFilter.class);

    public void init(FilterConfig filterConfig) throws ServletException {}

    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse
            , FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        if (!isSkip(request.getRequestURL().toString())) {//non static URI
            String method = request.getMethod();
            if("post".equalsIgnoreCase(method)){
                request.setCharacterEncoding(AdminConstants.DEFAULT_CHARACTER);
            }

            if (log.isInfoEnabled()) {
                log.info("请求 => {}", parseRequest(request));
            }
        }
        filterChain.doFilter(request, servletResponse);
    }

    public void destroy() {}

    /**
     * 输出所有请求和请求的参数
     *
     * @param request 请求
     * @return 请求输出
     */
    private static String parseRequest(HttpServletRequest request) {
        Map map = request.getParameterMap();
        String result = request.getRequestURL().toString();
        StringBuilder params = new StringBuilder();
        params.append(result);
        params.append(" \t params:{");
        int index = 0;
        for (Object name : map.keySet()) {
            if(index > 0){
                params.append(",");
            }
            index++;
            params.append(name);
            params.append("=");
            params.append(Arrays.toString((String[]) map.get(name)));

        }
        params.append("}");
        return params.toString();
    }

    /**
     * 是否静态资源
     *
     * @param url url地址
     * @return true/false
     */
    private boolean isSkip(String url) {
        return StringRegexUtils.isStaticResource(url);
    }
}