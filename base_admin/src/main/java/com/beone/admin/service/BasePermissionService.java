package com.beone.admin.service;

import com.base.ISuperService;
import com.beone.admin.entity.BasePermission;
import com.beone.admin.utils.PaginationGatagridTable;

import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * @Title 运维数据_系统菜单功能权限表 服务类
 * @Author 覃球球
 * @Version 1.0 on 2018-01-25
 * @Copyright 长笛龙吟
 */
public interface BasePermissionService extends ISuperService<BasePermission> {

    /**
     * 根据用户ID 获取权限菜单信息
     *
     * @param userId     用户ID
     * @param actionType page  表示只显示页面权限   all  显示所有权限
     * @return
     */
    List<BasePermission> getPermissionsesByUserId(String actionType, Integer userId);

    /**
     * 根据角色Id 获取权限菜单信息
     *
     * @param roleId
     * @return
     */
    List<BasePermission> getPermissionsesByRoleId(Integer roleId);

    /**
     * 获取所有权限菜单信息
     *
     * @param nodeType page  表示只显示页面权限   all  显示所有权限
     * @return
     */
    List<BasePermission> getAllPermissionses(String nodeType);


    /**
     * 显示所有可见的菜单权限
     *
     * @param nodeType page 页面权限  all 所有权限
     * @param used     used 可见权限  all 所有权限
     * @return 所有菜单权限
     */
    Collection<BasePermission> showPermissionMenuTree(String nodeType, String used);

    /**
     * 构建资源权限树结构
     *
     * @param permissionses
     * @param isExpand      true  展开  false  不展开
     * @return
     */
    Collection<BasePermission> getPermissionsTree(List<BasePermission> permissionses, boolean isExpand);


    /**
     * 分页显示后台权限列表
     *
     * @param id       权限ID
     * @param nodeName
     * @param currPage 当前页码
     * @param pageSize 每页显示记录数
     * @return
     */
    PaginationGatagridTable getPermissionPagination(String id, String nodeName, int currPage, int pageSize);

    /**
     * 保存权限信息
     *
     * @param access
     * @return
     */
    boolean savePermissions(BasePermission access);

    /**
     * 获取菜单面包屑
     *
     * @return
     */
    Map<String, String> getPermissionsBreads();

    /**
     * 根据nodeId 获取权限信息
     * @param nodeId
     * @return
     */
    BasePermission getPermissionByNodeId(Integer nodeId);
}
