package com.beone.admin.controller.base;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;

/**
 * @Title 运维数据_角色对应的权限配置 前端控制器
 * @Author 覃球球
 * @Version 1.0 on 2018-01-25
 * @Copyright 长笛龙吟
 */
@Controller
@RequestMapping("/system/operationAccessRole")
public class BasePermissionRoleController {

}

