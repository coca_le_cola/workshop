package com.beone.admin.config;

import com.beone.admin.annotation.LogAspect;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


/**
 * @title  日志拦截器配置
 * @Author 覃球球
 * @Version 1.0 on 2017/12/20.
 * @Copyright 长笛龙吟
 */
@Configuration
public class LogConfig {

  @Bean(name = "logAspect")
  public LogAspect getLogAspect(){
    return new LogAspect();
  }

}
