package com.beone.admin.config;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.convert.converter.Converter;
import org.springframework.core.convert.support.GenericConversionService;
import org.springframework.web.bind.support.ConfigurableWebBindingInitializer;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerAdapter;

import javax.annotation.PostConstruct;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @title  日期转化器
 * @Author 覃球球
 * @Version 1.0 on 2018/9/7.
 * @Copyright 贝旺科技
 */
@Configuration
public class DateConfig {

    @Autowired
    private RequestMappingHandlerAdapter handlerAdapter;

    @PostConstruct
    public void initEditableValidation(){
        ConfigurableWebBindingInitializer initializer = (ConfigurableWebBindingInitializer)
                handlerAdapter.getWebBindingInitializer();
        if(initializer.getConversionService() != null){
            GenericConversionService service = (GenericConversionService) initializer.getConversionService();
            service.addConverter(new StringToDateConverter());
        }
    }

    class StringToDateConverter implements Converter<String,Date> {
        private static final String dateFormat = "yyyy-MM-dd HH:mm:ss";
        private static final String shortDateFormat = "yyyy-MM-dd";
        private static final String dateFormat2 = "yyyy/MM/dd HH:mm:ss";
        private static final String shortDateFormat2 = "yyyy/MM/dd";

        @Override
        public Date convert(String source) {
            if (StringUtils.isBlank(source)) {
                return null;
            }
            source = source.trim();
            try {
                SimpleDateFormat formatter;
                if (source.contains("-")) {
                    if (source.contains(":")) {
                        formatter = new SimpleDateFormat(dateFormat);
                    } else {
                        formatter = new SimpleDateFormat(shortDateFormat);
                    }
                    Date dtDate = formatter.parse(source);
                    return dtDate;
                } else if (source.contains("/")) {
                    if (source.contains(":")) {
                        formatter = new SimpleDateFormat(dateFormat2);
                    } else {
                        formatter = new SimpleDateFormat(shortDateFormat2);
                    }
                    Date dtDate = formatter.parse(source);
                    return dtDate;
                }
            } catch (Exception e) {
                throw new RuntimeException(String.format("parser %s to Date fail", source));
            }
            throw new RuntimeException(String.format("parser %s to Date fail", source));
        }
    }

}
